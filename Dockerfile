FROM gradle:jdk11 as builder
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon

FROM openjdk:11-jre-slim
RUN mkdir /app
COPY --from=builder /home/gradle/src/build/libs/userapi-*.jar /app/userapi.jar

WORKDIR /app

ENTRYPOINT ["java", "-jar", "/app/userapi.jar"]