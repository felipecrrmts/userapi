# Cashier Tech Test

## Tech/Framework used
<b>Scaffolding</b>
- [SPRING INITIALIZR] (https://start.spring.io)

<b>Built with</b>
- [JDK 11] (http://www.oracle.com/technetwork/java/javase/overview/index.html)
- [Gradle] (https://gradle.org)
- [Spring Boot] (https://spring.io/projects/spring-boot)
- [Junit] (https://junit.org/junit5)
- [Mokito] (https://site.mockito.org)
- [SpringFox] (https://springfox.github.io/springfox/)
- [MySql] (https://www.mysql.com/)
- [H2database] (https://www.h2database.co)
    
## Assumptions
- Nickname and emails are unique.
- Server-Side Events were used to notify other interested services who need to subscribe first
- H2database was used for repository tests
- Search is made only in the right side of the string (field like 'string%')

### Validations
- FirstName: Only letters and numbers
- LastName: Only letters and numbers
- NickName: Only letters and numbers
- Password: At least 1 lowercase, 1 uppercase, 1 number and 1 special character
- Email: Regex to restrict no. of characters in top level domain
- Country: Country Alpha-2 Code

### Extensions or Improvements
- Better authentication (Oauth2)
- loading users and password from a secrets management and data protection (ex: HashiCorp Vault) 
- Depending on the need change the Server-Side Events for a more robust solution (ex: Message Broker/Topic)
- Creating pipelines for deployment
- Exposing metrics via some reports tool (Ex: kibana)

## Packaging and running

Commands below must be executed from the project root

### Build

`make run` or `docker-compose up --build race`

### Run

`make stop` or `docker-compose down`

## Swagger

- Swagger UI `http://localhost:8080/swagger-ui.html`
- Swagger docs `http://localhost:8080/v2/api-docs`

## Health

`http://localhost:8080/actuator/health`

## Postman

There is a postman collection file in the project root for manual tests
- userapi.postman_collection.json