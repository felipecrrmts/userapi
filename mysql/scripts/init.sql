create database if not exists userapi;
use userapi;

create table user
(
    uuid            binary(16) PRIMARY KEY,
    first_name      varchar(20) not null,
    last_name       varchar(20) not null,
    nick_name       varchar(20) not null,
    password        varchar(128) null,
    email           varchar(128) null,
    country         varchar(2) null);
INSERT INTO user (uuid, first_name, last_name, nick_name, password, email, country)
VALUES (UUID_TO_BIN(UUID()), 'Keeper', 'ofTheLight', 'Ezalor', 'asdasdasdasd', 'keeper.ofthelight@dota.com', 'GB');
INSERT INTO user (uuid, first_name, last_name, nick_name, password, email, country)
VALUES (UUID_TO_BIN(UUID()), 'Wraith', 'King', 'SkTheOld', 'teteetasdas', 'oldbones@dota.com', 'GB');
INSERT INTO user (uuid, first_name, last_name, nick_name, password, email, country)
VALUES (UUID_TO_BIN(UUID()), 'Natures', 'Prophet', 'Furion', 'hihihihiahhohoho', 'furion322@dota.com', 'BR');