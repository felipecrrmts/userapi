package com.faceit.user.advice;

import com.faceit.user.exception.TakenException;
import com.faceit.user.exception.UserNotFoundException;
import com.faceit.user.resource.ErrorResponse;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice(annotations = RestController.class)
@Slf4j
public class RestExceptionHandler {

  @ExceptionHandler(TakenException.class)
  @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
  @ResponseBody
  public ErrorResponse handleBlockedRegisterException(TakenException exception) {
    log.info("User was not found by using id: {}",
        String.join(", ", exception.getErrorsAsMessages()));
    return new ErrorResponse(exception.getErrorsAsMessages());
  }

  @ExceptionHandler(UserNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ResponseBody
  public ErrorResponse handleTodoEntryNotFound(UserNotFoundException exception) {
    log.info("User was not found by using id: {}", exception.getMessage());
    return new ErrorResponse(exception.getErrorsAsMessages());
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ErrorResponse handleValidationErrors(MethodArgumentNotValidException ex) {
    BindingResult result = ex.getBindingResult();
    List<FieldError> fieldErrors = result.getFieldErrors();
    log.info("Found {} validation errors", fieldErrors.size());
    return constructValidationErrors(fieldErrors);
  }

  private ErrorResponse constructValidationErrors(List<FieldError> fieldErrors) {
    return new ErrorResponse(fieldErrors.stream()
        .map(fieldError -> getErrorAsText(fieldError.getField(), fieldError.getDefaultMessage()))
        .collect(Collectors.toList()));
  }

  private String getErrorAsText(String field, String message) {
    return String.format("%s : %s", field, message);
  }
}
