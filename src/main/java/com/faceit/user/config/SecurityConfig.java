package com.faceit.user.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableConfigurationProperties(SecurityUserProperties.class)
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  private static final String[] PUBLIC_URIS = {
      "/swagger-ui.html",
      "/v2/api-docs",
      "/swagger-resources/**",
      "/webjars/**",
      "/actuator/**",
  };
  private final CustomAuthenticationEntryPoint customAuthenticationEntryPoint;
  private final SecurityUserProperties securityUser;

  public SecurityConfig(
      CustomAuthenticationEntryPoint customAuthenticationEntryPoint,
      SecurityUserProperties securityUser) {
    this.customAuthenticationEntryPoint = customAuthenticationEntryPoint;
    this.securityUser = securityUser;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .csrf().disable()
        .authorizeRequests()
        .antMatchers(PUBLIC_URIS).permitAll()
        .anyRequest().authenticated()
        .and()
        .httpBasic()
        .authenticationEntryPoint(customAuthenticationEntryPoint);
  }

  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    auth.inMemoryAuthentication()
        .withUser(securityUser.getUsername())
        .password(passwordEncoder().encode(securityUser.getPassword()))
        .roles(securityUser.getRole());
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }
}
