package com.faceit.user.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("security.user")
@Data
public class SecurityUserProperties {

  private String username;
  private String password;
  private String role;
}
