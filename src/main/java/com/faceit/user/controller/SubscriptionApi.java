package com.faceit.user.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@Api(tags = {"User"})
@RequestMapping("/users")
public interface SubscriptionApi {

  @ApiOperation(
      value = "subscription endpoint to receive updates when an user is updated",
      nickname = "subscribeToUser",
      tags = {"User"}
  )
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Ok")
  })
  @GetMapping(value = "/subscribe", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
  SseEmitter subscribeToUser();

}
