package com.faceit.user.controller;

import com.faceit.user.service.SseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@Slf4j
@RestController
public class SubscriptionController implements SubscriptionApi {

  private final SseService sseService;

  public SubscriptionController(SseService sseService) {
    this.sseService = sseService;
  }

  @Override
  public SseEmitter subscribeToUser() {
    log.info("subscribed");
    return sseService.subscribe();
  }

}
