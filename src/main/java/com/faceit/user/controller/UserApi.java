package com.faceit.user.controller;

import com.faceit.user.resource.SearchCriteria;
import com.faceit.user.resource.UserResource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import java.util.UUID;
import javax.validation.Valid;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Api(tags = {"User"})
@Validated
@RequestMapping("/users")
public interface UserApi {

  @ApiOperation(
      value = "used to create a user in the application",
      nickname = "newUser",
      tags = {"User"}
  )
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Created"),
      @ApiResponse(code = 400, message = "Bad request"),
      @ApiResponse(code = 422, message = "Unprocessable Entity"),
  })
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<Void> newUser(
      @ApiParam(value = "User information needed to create a user registration", required = true)
      @Valid @RequestBody UserResource user);

  @ApiOperation(
      value = "list all users in the application",
      nickname = "all",
      tags = {"User"}
  )
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Ok"),
      @ApiResponse(code = 400, message = "Bad request"),
      @ApiResponse(code = 422, message = "Unprocessable Entity"),
  })
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<List<UserResource>> all(SearchCriteria searchCriteria);

  @ApiOperation(
      value = "used to create a user in the application",
      nickname = "one",
      tags = {"User"}
  )
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Created"),
      @ApiResponse(code = 404, message = "Not found"),
  })
  @GetMapping(value = "/{uuid}", produces = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<UserResource> one(@PathVariable("uuid") UUID uuid);

  @ApiOperation(
      value = "update a user in the application",
      nickname = "update",
      tags = {"User"}
  )
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Ok"),
      @ApiResponse(code = 400, message = "Bad request"),
      @ApiResponse(code = 422, message = "Conflict"),
  })
  @PutMapping(value = "/{uuid}",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<UserResource> update(
      @PathVariable("uuid") UUID uuid,
      @Valid @RequestBody UserResource user);

  @ApiOperation(
      value = "delete a user in the application",
      nickname = "delete",
      tags = {"User"}
  )
  @ApiResponses(value = {
      @ApiResponse(code = 204, message = "Ok"),
      @ApiResponse(code = 404, message = "Not found"),
  })
  @DeleteMapping(value = "/{uuid}",
      produces = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<Void> delete(@PathVariable("uuid") UUID uuid);

}