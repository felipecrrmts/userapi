package com.faceit.user.controller;

import com.faceit.user.resource.SearchCriteria;
import com.faceit.user.resource.UserResource;
import com.faceit.user.service.UserService;
import java.net.URI;
import java.util.List;
import java.util.UUID;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@Slf4j
@RestController
public class UserController implements UserApi {

  private final UserService userService;

  public UserController(UserService userService) {
    this.userService = userService;
  }

  @Override
  public ResponseEntity<Void> newUser(@Valid UserResource user) {
    log.info("newUser started");
    return ResponseEntity.created(getUriForLocation(userService.create(user))).build();
  }

  @Override
  public ResponseEntity<List<UserResource>> all(SearchCriteria searchCriteria) {
    log.info("allUser started with filters: {}", searchCriteria.getLog());
    return ResponseEntity.ok(userService.all(searchCriteria));
  }

  @Override
  public ResponseEntity<UserResource> one(UUID uuid) {
    log.info("oneUser started with id: {}", uuid);
    return ResponseEntity.ok(userService.one(uuid));
  }

  @Override
  public ResponseEntity<UserResource> update(UUID uuid, @Valid UserResource user) {
    log.info("updateUser started with id: {}", uuid);
    return new ResponseEntity<>(
        getHeadersWithLocationSet(userService.update(uuid, user)),
        HttpStatus.OK);
  }

  @Override
  public ResponseEntity<Void> delete(UUID uuid) {
    log.info("deleteUser started with id: {}", uuid);
    userService.delete(uuid);
    return ResponseEntity.noContent().build();
  }

  private URI getUriForLocation(UUID uuid) {
    return ServletUriComponentsBuilder.fromCurrentRequest()
        .path("/{id}")
        .buildAndExpand(uuid)
        .toUri();
  }

  private HttpHeaders getHeadersWithLocationSet(UUID uuid) {
    HttpHeaders headers = new HttpHeaders();
    headers.setLocation(getUriForLocation(uuid));
    return headers;
  }
}
