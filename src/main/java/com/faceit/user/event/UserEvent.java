package com.faceit.user.event;

import com.faceit.user.resource.UserResource;
import lombok.Value;

@Value
public class UserEvent {

  UserResource userResource;

}
