package com.faceit.user.exception;

import java.util.List;
import java.util.stream.Collectors;

public class TakenException extends RuntimeException {

  private final List<String> errors;

  public TakenException(List<String> errors) {
    this.errors = errors;
  }

  public List<String> getErrorsAsMessages() {
    return errors.stream()
        .map(error -> String.format("%s has been taken", error))
        .collect(Collectors.toList());
  }
}
