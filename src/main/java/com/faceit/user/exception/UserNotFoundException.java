package com.faceit.user.exception;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class UserNotFoundException extends RuntimeException {

  public UserNotFoundException(UUID uuid) {
    super(uuid.toString());
  }

  public List<String> getErrorsAsMessages() {
    return Collections.singletonList(
        String.format("User was not found by using id: %s", this.getMessage()));
  }

}
