package com.faceit.user.repository;

import com.faceit.user.model.User;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserRepository extends JpaRepository<User, UUID>, JpaSpecificationExecutor<User> {

  boolean existsUserByNickName(String nickName);

  boolean existsUserByEmail(String email);

}