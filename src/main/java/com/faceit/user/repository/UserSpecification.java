package com.faceit.user.repository;

import com.faceit.user.model.User;
import com.faceit.user.resource.SearchCriteria;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;

public class UserSpecification implements Specification<User> {

  private static final String FIRST_NAME_KEY = "firstName";
  private static final String LAST_NAME_KEY = "lastName";
  private static final String NICK_NAME_KEY = "nickName";
  private static final String EMAIL_KEY = "email";
  private static final String COUNTRY_KEY = "country";
  private static final long serialVersionUID = 6177546801682290069L;
  private final SearchCriteria searchCriteria;

  public UserSpecification(SearchCriteria searchCriteria) {
    this.searchCriteria = searchCriteria;
  }

  @Override
  public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query,
      CriteriaBuilder criteriaBuilder) {

    List<Predicate> predicates = new ArrayList<>();

    if (searchCriteria.getFirstName() != null) {
      predicates.add(criteriaBuilder.like(
          criteriaBuilder.lower(root.get(FIRST_NAME_KEY)),
          searchCriteria.getFirstName().toLowerCase() + "%"));
    }
    if (searchCriteria.getLastName() != null) {
      predicates.add(criteriaBuilder.like(
          criteriaBuilder.lower(root.get(LAST_NAME_KEY)),
          searchCriteria.getLastName().toLowerCase() + "%"));
    }
    if (searchCriteria.getNickName() != null) {
      predicates.add(criteriaBuilder.like(
          criteriaBuilder.lower(root.get(NICK_NAME_KEY)),
          searchCriteria.getNickName().toLowerCase() + "%"));
    }
    if (searchCriteria.getEmail() != null) {
      predicates.add(criteriaBuilder.like(
          criteriaBuilder.lower(root.get(EMAIL_KEY)),
          searchCriteria.getEmail().toLowerCase() + "%"));
    }
    if (searchCriteria.getCountry() != null) {
      predicates.add(criteriaBuilder.like(
          criteriaBuilder.lower(root.get(COUNTRY_KEY)),
          searchCriteria.getCountry().toLowerCase() + "%"));
    }
    return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
  }

  public Sort orderBy() {
    return Sort.by(Sort.Direction.ASC, FIRST_NAME_KEY)
        .and(Sort.by(Direction.ASC, LAST_NAME_KEY)
            .and(Sort.by(Direction.ASC, NICK_NAME_KEY)
                .and(Sort.by(Direction.ASC, EMAIL_KEY)
                    .and(Sort.by(Direction.ASC, COUNTRY_KEY)))));
  }
}
