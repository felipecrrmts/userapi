package com.faceit.user.resource;

import java.util.List;
import lombok.Value;

@Value
public class ErrorResponse {

  List<String> errorMessages;
}
