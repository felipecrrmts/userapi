package com.faceit.user.resource;

import com.faceit.user.resource.validator.CountryCode;
import io.swagger.annotations.ApiParam;
import lombok.Value;
import org.springframework.validation.annotation.Validated;

@Value
@Validated
public class SearchCriteria {

  @ApiParam(value = "First Name")
  String firstName;

  @ApiParam(value = "Last Name")
  String lastName;

  @ApiParam(value = "Nick Name")
  String nickName;

  @ApiParam(value = "User email")
  String email;

  @ApiParam(value = "Country Alpha-2 Code")
  @CountryCode
  String country;

  public String getLog() {
    return String.format("firstName-> %s, lastName-> %s, nickName-> %s, email-> %s, country-> %s",
        firstName, lastName, nickName, email, country);
  }
}
