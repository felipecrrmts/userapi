package com.faceit.user.resource;

import com.faceit.user.resource.validator.CountryCode;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.annotations.ApiParam;
import java.util.UUID;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import lombok.Value;
import org.springframework.validation.annotation.Validated;

@Value
@Validated
public class UserResource {

  @ApiParam(value = "Id of the user")
  @JsonInclude(Include.NON_NULL)
  UUID uuid;

  @ApiParam(value = "First Name")
  @Pattern(regexp = "^[a-zA-Z ,.'-]{2,20}+$")
  String firstName;

  @ApiParam(value = "Last Name")
  @Pattern(regexp = "^[a-zA-Z ,.'-]{2,20}+$")
  String lastName;

  @ApiParam(value = "Nick Name")
  @Pattern(regexp = "^[a-zA-Z0-9]{6,20}+$")
  String nickName;

  @ApiParam(value = "User password")
  @Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")
  String password;

  @ApiParam(value = "User email")
  @Email
  String email;

  @ApiParam(value = "Country Alpha-2 Code")
  @CountryCode
  String country;

  public static UserResource newUserResource(
      String firstName,
      String lastName,
      String nickName,
      String password,
      String email,
      String country) {
    return new UserResource(null, firstName, lastName, nickName, password, email, country);
  }

}
