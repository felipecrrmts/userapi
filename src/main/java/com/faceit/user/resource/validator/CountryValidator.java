package com.faceit.user.resource.validator;

import java.util.Locale;
import java.util.Set;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CountryValidator implements ConstraintValidator<CountryCode, String> {

  private final Set<String> countryCodeSet;

  public CountryValidator() {
    this.countryCodeSet = Set.of(Locale.getISOCountries());
  }

  @Override
  public boolean isValid(String code, ConstraintValidatorContext context) {
    return countryCodeSet.contains(code);
  }
}