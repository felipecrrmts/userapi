package com.faceit.user.service;

import com.faceit.user.event.UserEvent;
import org.springframework.context.event.EventListener;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

public interface SseService {

  SseEmitter subscribe();

  @EventListener
  void onPublish(UserEvent event);
}
