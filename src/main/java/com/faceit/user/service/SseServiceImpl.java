package com.faceit.user.service;

import com.faceit.user.event.UserEvent;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@Slf4j
@Service
public class SseServiceImpl implements SseService {

  private static final long TIMEOUT = Long.MAX_VALUE;
  private final Set<SseEmitter> emitters = new HashSet<>();

  @Override
  public SseEmitter subscribe() {
    SseEmitter sseEmitter = new SseEmitter(TIMEOUT);
    config(sseEmitter);
    emitters.add(sseEmitter);
    log.info("Emitter " + sseEmitter.toString() + " SUBSCRIBED!");
    return sseEmitter;
  }

  @Override
  @EventListener(classes = UserEvent.class)
  public void onPublish(UserEvent event) {
    emitters.forEach(emitter -> {
      try {
        log.debug("Sending message through emitter " + emitter.toString());
        emitter.send(event.getUserResource());
      } catch (IOException e) {
        log.error("Error in emitter " + emitter + " while sending message");
        emitters.remove(emitter);
      }
    });
  }

  private void config(SseEmitter emitter) {
    emitter.onCompletion(() -> {
      log.debug("Emitter " + emitter.toString() + " COMPLETED!");
      emitters.remove(emitter);
    });
    emitter.onTimeout(() -> {
      log.debug("Emitter " + emitter.toString() + " TIMEOUT!");
      emitters.remove(emitter);
    });
  }

}
