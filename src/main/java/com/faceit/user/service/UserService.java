package com.faceit.user.service;

import com.faceit.user.resource.SearchCriteria;
import com.faceit.user.resource.UserResource;
import java.util.List;
import java.util.UUID;

public interface UserService {

  UUID create(UserResource user);

  List<UserResource> all(SearchCriteria searchCriteria);

  UserResource one(UUID uuid);

  UUID update(UUID uuid, UserResource user);

  void delete(UUID uuid);
}
