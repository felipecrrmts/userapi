package com.faceit.user.service;

import com.faceit.user.exception.TakenException;
import com.faceit.user.exception.UserNotFoundException;
import com.faceit.user.model.User;
import com.faceit.user.repository.UserRepository;
import com.faceit.user.repository.UserSpecification;
import com.faceit.user.resource.SearchCriteria;
import com.faceit.user.resource.UserResource;
import com.faceit.user.service.mapper.UserMapper;
import com.faceit.user.event.UserEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

  private final ApplicationEventPublisher applicationEventPublisher;
  private final UserRepository userRepository;
  private final UserMapper userMapper;

  public UserServiceImpl(
      ApplicationEventPublisher applicationEventPublisher,
      UserRepository userRepository,
      UserMapper userMapper) {
    this.applicationEventPublisher = applicationEventPublisher;
    this.userRepository = userRepository;
    this.userMapper = userMapper;
  }

  @Override
  public UUID create(UserResource userResource) {

    log.debug("create new user");
    List<String> errors = checkBusinessRules(userResource);
    if (errors.isEmpty()) {
      return userRepository.save(userMapper.mapToNew(userResource)).getUuid();
    }
    throw new TakenException(errors);
  }

  private List<String> checkBusinessRules(UserResource userResource) {
    List<String> errors = new ArrayList<>();
    if (userRepository.existsUserByEmail(userResource.getEmail())) {
      errors.add(userResource.getEmail());
    }
    if (userRepository.existsUserByNickName(userResource.getNickName())) {
      errors.add(userResource.getNickName());
    }
    return errors;
  }

  @Override
  public List<UserResource> all(SearchCriteria searchCriteria) {
    log.debug("Searching users");
    UserSpecification userSpecification = new UserSpecification(searchCriteria);
    return userRepository.findAll(userSpecification, userSpecification.orderBy()).stream()
        .map(userMapper::mapToUserResource)
        .collect(Collectors.toList());
  }

  @Override
  public UserResource one(UUID uuid) {
    log.debug("one user with id: {}", uuid);
    return userRepository.findById(uuid)
        .map(userMapper::mapToUserResource)
        .orElseThrow(() -> new UserNotFoundException(uuid));
  }

  @Override
  public UUID update(UUID uuid, UserResource userResource) {
    log.debug("update user with id: {}", uuid);
    List<String> errors = userRepository.findById(uuid)
        .map(user -> checkBusinessRules(user, userResource))
        .orElseThrow(() -> new UserNotFoundException(uuid));
    if (errors.isEmpty()) {
      User user = userRepository.save(userMapper.mapToUser(uuid, userResource));
      applicationEventPublisher.publishEvent(new UserEvent(userResource));
      return user.getUuid();
    }
    throw new TakenException(errors);
  }

  @Override
  public void delete(UUID uuid) {
    log.debug("delete user with id: {}", uuid);
    userRepository.findById(uuid)
        .ifPresentOrElse(userRepository::delete, () -> {
          throw new UserNotFoundException(uuid);
        });
  }

  private List<String> checkBusinessRules(User user, UserResource userResource) {
    List<String> errors = new ArrayList<>();
    if (!user.getEmail().equalsIgnoreCase(userResource.getEmail())
        && userRepository.existsUserByEmail(userResource.getEmail())) {
      errors.add(userResource.getEmail());
    }
    if (!user.getNickName().equals(userResource.getNickName())
        && userRepository.existsUserByNickName(userResource.getNickName())) {
      errors.add(userResource.getNickName());
    }
    return errors;
  }
}
