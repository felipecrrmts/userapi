package com.faceit.user.service.mapper;

import com.faceit.user.model.User;
import com.faceit.user.resource.UserResource;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

  @Autowired
  private final PasswordEncoder passwordEncoder;

  public UserMapper(PasswordEncoder passwordEncoder) {
    this.passwordEncoder = passwordEncoder;
  }

  public UserResource mapToUserResource(User user) {
    return new UserResource(
        user.getUuid(),
        user.getFirstName(),
        user.getLastName(),
        user.getNickName(),
        user.getPassword(),
        user.getEmail(),
        user.getCountry());
  }

  public User mapToNew(UserResource user) {
    return new User(
        null,
        user.getFirstName(),
        user.getLastName(),
        user.getNickName(),
        passwordEncoder.encode(user.getPassword()),
        user.getEmail(),
        user.getCountry());
  }

  public User mapToUser(UUID uuid, UserResource userResource) {

    return new User(
        uuid,
        userResource.getFirstName(),
        userResource.getLastName(),
        userResource.getNickName(),
        passwordEncoder.encode(userResource.getPassword()),
        userResource.getEmail(),
        userResource.getCountry());
  }
}
