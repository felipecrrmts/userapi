package com.faceit.user.controller;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.faceit.user.config.CustomAuthenticationEntryPoint;
import com.faceit.user.config.SecurityConfig;
import com.faceit.user.exception.TakenException;
import com.faceit.user.exception.UserNotFoundException;
import com.faceit.user.resource.SearchCriteria;
import com.faceit.user.resource.UserResource;
import com.faceit.user.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(UserController.class)
@Import({
    SecurityConfig.class,
    CustomAuthenticationEntryPoint.class
})
class UserApiTest {

  private static final String USER_URI = "/users";
  private static final String FIRST_NAME_KEY = "firstName";
  private static final String FIRST_NAME = "Bone";
  private static final String LAST_NAME_KEY = "lastName";
  private static final String LAST_NAME = "Fletcher";
  private static final String NICK_NAME_KEY = "nickName";
  private static final String NICK_NAME = "Clinkz";
  private static final String PASSWORD_KEY = "password";
  private static final String PASSWORD = "P@ssword322";
  private static final String EMAIL_KEY = "email";
  private static final String EMAIL = "little.sk@dota.com";
  private static final String COUNTRY_KEY = "country";
  private static final String COUNTRY = "BR";

  @Autowired
  private MockMvc mvc;
  @Autowired
  private ObjectMapper objectMapper;
  @MockBean
  private UserService userService;

  private UserResource getUserResource() {
    return UserResource.newUserResource(
        "Bone",
        "Fletcher",
        "Clinkz",
        "asdasdasd",
        "little.sk@dota.com",
        "CN");
  }

  @Test
  void whenNotUsingAuthenticationThenReturnsStatus401() throws Exception {

    String body = validUser();
    mvc.perform(post(USER_URI)
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .content(body))
        .andExpect(status().isUnauthorized());

    verifyNoInteractions(userService);
  }

  @WithUserDetails
  @Test
  void whenNewUserIsValidThenReturnsStatus201() throws Exception {

    String body = validUser();
    UserResource userResource = objectMapper.readValue(body, UserResource.class);
    UUID uuid = UUID.randomUUID();
    when(userService.create(userResource)).thenReturn(uuid);

    mvc.perform(post(USER_URI)
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .content(body))
        .andDo(print())
        .andExpect(status().isCreated())
        .andExpect(header().string(HttpHeaders.LOCATION,
            containsString("/users/" + uuid.toString())));

    verify(userService).create(userResource);
    verifyNoMoreInteractions(userService);
  }

  @WithUserDetails
  @Test
  void whenNewUserIsInValidThenReturnsStatus400() throws Exception {

    String body = inValidUser();

    mvc.perform(post(USER_URI)
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .content(body))
        .andDo(print())
        .andExpect(status().isBadRequest());

    verifyNoInteractions(userService);
  }

  @WithUserDetails
  @Test
  void whenNewUserNickNameIsTakenThenReturnsStatus422() throws Exception {

    String body = validUser();
    UserResource userResource = objectMapper.readValue(body, UserResource.class);
    doThrow(new TakenException(Collections.singletonList(userResource.getNickName())))
        .when(userService).create(userResource);

    mvc.perform(post(USER_URI)
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .content(body))
        .andExpect(status().isUnprocessableEntity())
        .andExpect(jsonPath("$.errorMessages", hasSize(1)));

    verify(userService).create(userResource);
    verifyNoMoreInteractions(userService);
  }

  @WithUserDetails
  @Test
  void whenNewUserNickNameAndEmailIsTakenThenReturnsStatus422() throws Exception {

    String body = validUser();
    UserResource userResource = objectMapper.readValue(body, UserResource.class);
    doThrow(new TakenException(List.of(userResource.getNickName(), userResource.getEmail())))
        .when(userService).create(userResource);

    mvc.perform(post(USER_URI)
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .content(body))
        .andExpect(status().isUnprocessableEntity())
        .andExpect(jsonPath("$.errorMessages", hasSize(2)));

    verify(userService).create(userResource);
    verifyNoMoreInteractions(userService);
  }

  @WithUserDetails
  @Test
  void whenGetUserWithValidUuidThenReturnsStatus200() throws Exception {

    UUID uuid = UUID.randomUUID();
    UserResource userResource = getUserResource();
    when(userService.one(uuid)).thenReturn(userResource);

    mvc.perform(get(USER_URI + "/{uuid}", uuid.toString()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.firstName", equalTo(userResource.getFirstName())))
        .andExpect(jsonPath("$.lastName", equalTo(userResource.getLastName())))
        .andExpect(jsonPath("$.nickName", equalTo(userResource.getNickName())))
        .andExpect(jsonPath("$.country", equalTo(userResource.getCountry())))
        .andExpect(jsonPath("$.email", equalTo(userResource.getEmail())))
        .andExpect(jsonPath("$.password", equalTo(userResource.getPassword())));

    verify(userService).one(uuid);
    verifyNoMoreInteractions(userService);
  }

  @WithUserDetails
  @Test
  void whenGetUserWithUnknownUuidThenReturnsStatus404() throws Exception {

    UUID uuid = UUID.randomUUID();
    doThrow(new UserNotFoundException(uuid)).when(userService).one(uuid);

    mvc.perform(get(USER_URI + "/{uuid}", uuid.toString()))
        .andExpect(status().isNotFound())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON));

    verify(userService).one(uuid);
    verifyNoMoreInteractions(userService);
  }

  @WithUserDetails
  @Test
  void whenDeleteUserWithValidUuidThenReturnsStatus204() throws Exception {

    UUID uuid = UUID.randomUUID();
    doNothing().when(userService).delete(uuid);

    mvc.perform(delete(USER_URI + "/{uuid}", uuid.toString()))
        .andExpect(status().isNoContent());

    verify(userService).delete(uuid);
    verifyNoMoreInteractions(userService);
  }

  @WithUserDetails
  @Test
  void whenDeleteUserWithUnknownUuidThenReturnsStatus404() throws Exception {

    UUID uuid = UUID.randomUUID();
    doThrow(new UserNotFoundException(uuid)).when(userService).delete(uuid);

    mvc.perform(delete(USER_URI + "/{uuid}", uuid.toString()))
        .andExpect(status().isNotFound())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON));

    verify(userService).delete(uuid);
    verifyNoMoreInteractions(userService);
  }

  @WithUserDetails
  @Test
  void whenUpdateUserIsValidThenReturnsStatus200() throws Exception {

    String body = validUser();
    UserResource userResource = objectMapper.readValue(body, UserResource.class);
    UUID uuid = UUID.randomUUID();
    when(userService.update(uuid, userResource)).thenReturn(uuid);

    mvc.perform(put(USER_URI + "/{uuid}", uuid.toString())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .content(body))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(header().string(HttpHeaders.LOCATION,
            containsString("/users/" + uuid.toString())));

    verify(userService).update(uuid, userResource);
    verifyNoMoreInteractions(userService);
  }

  @WithUserDetails
  @Test
  void whenUpdateUserIsInValidThenReturnsStatus400() throws Exception {

    String body = inValidUser();
    UUID uuid = UUID.randomUUID();

    mvc.perform(put(USER_URI + "/{uuid}", uuid.toString())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .content(body))
        .andDo(print())
        .andExpect(status().isBadRequest());

    verifyNoInteractions(userService);
  }

  @WithUserDetails
  @Test
  void whenUpdateUserNickNameIsTakenThenReturnsStatus422() throws Exception {

    String body = validUser();
    UUID uuid = UUID.randomUUID();
    UserResource userResource = objectMapper.readValue(body, UserResource.class);
    doThrow(new TakenException(Collections.singletonList(userResource.getNickName())))
        .when(userService).update(uuid, userResource);

    mvc.perform(put(USER_URI + "/{uuid}", uuid.toString())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .content(body))
        .andExpect(status().isUnprocessableEntity())
        .andExpect(jsonPath("$.errorMessages", hasSize(1)));

    verify(userService).update(uuid, userResource);
    verifyNoMoreInteractions(userService);
  }

  @WithUserDetails
  @Test
  void whenUpdateUserNickNameAndEmailIsTakenThenReturnsStatus422() throws Exception {

    String body = validUser();
    UUID uuid = UUID.randomUUID();
    UserResource userResource = objectMapper.readValue(body, UserResource.class);
    doThrow(new TakenException(List.of(userResource.getNickName(), userResource.getEmail())))
        .when(userService).update(uuid, userResource);

    mvc.perform(put(USER_URI + "/{uuid}", uuid.toString())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .content(body))
        .andExpect(status().isUnprocessableEntity())
        .andExpect(jsonPath("$.errorMessages", hasSize(2)));

    verify(userService).update(uuid, userResource);
    verifyNoMoreInteractions(userService);
  }

  @WithUserDetails
  @Test
  void whenGetUsersThenReturnsStatus200() throws Exception {

    when(userService.all(any(SearchCriteria.class)))
        .thenReturn(List.of(getUserResource(), getUserResource(), getUserResource()));

    mvc.perform(get(USER_URI))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.*", hasSize(3)));

    verify(userService).all(any(SearchCriteria.class));
    verifyNoMoreInteractions(userService);
  }

  private String validUser() {
    return payload(Map.of(
        FIRST_NAME_KEY, FIRST_NAME,
        LAST_NAME_KEY, LAST_NAME,
        NICK_NAME_KEY, NICK_NAME,
        PASSWORD_KEY, PASSWORD,
        EMAIL_KEY, EMAIL,
        COUNTRY_KEY, COUNTRY
    ));
  }

  private String inValidUser() {
    return payload(Map.of(
        FIRST_NAME_KEY, ":@~<>",
        LAST_NAME_KEY, ":@~<>",
        NICK_NAME_KEY, ":@~<>",
        PASSWORD_KEY, ".,.,.,.",
        EMAIL_KEY, "clinkz@dota..com",
        COUNTRY_KEY, "ZZ"
    ));
  }

  private String payload(Object from) {
    try {
      return new ObjectMapper().writeValueAsString(from);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
    return null;
  }
}