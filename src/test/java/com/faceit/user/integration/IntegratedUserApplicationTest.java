package com.faceit.user.integration;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.annotation.DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.faceit.user.model.User;
import com.faceit.user.repository.UserRepository;
import com.faceit.user.resource.UserResource;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@DirtiesContext(classMode = BEFORE_EACH_TEST_METHOD)
class IntegratedUserApplicationTest {

  private final static String URI = "/users";
  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private ObjectMapper objectMapper;
  @Autowired
  private UserRepository userRepository;

  private User newUser() {
    return new User(
        null,
        "Bone",
        "Fletcher",
        "Clinkz",
        "No1ceP@ssword",
        "little.sk@dota.com",
        "BR");
  }

  @WithUserDetails
  @Test
  void shouldSaveUserThenRetrieve() throws Exception {
    UserResource userResource = UserResource.newUserResource(
        "Arthas",
        "Menethil",
        "Omniknight",
        "N1ceP@ssword",
        "Arthas.Menethil@wow.com",
        "GB");
    MvcResult result = mockMvc.perform(post(URI)
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .content(objectMapper.writeValueAsString(userResource)))
        .andExpect(status().isCreated())
        .andReturn();
    String location = result.getResponse().getHeader(HttpHeaders.LOCATION);

    assertThat(location, is(notNullValue()));

    mockMvc.perform(get(location))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.firstName", equalTo(userResource.getFirstName())))
        .andExpect(jsonPath("$.lastName", equalTo(userResource.getLastName())))
        .andExpect(jsonPath("$.nickName", equalTo(userResource.getNickName())))
        .andExpect(jsonPath("$.country", equalTo(userResource.getCountry())))
        .andExpect(jsonPath("$.email", equalTo(userResource.getEmail())));
  }

  @WithUserDetails
  @Test
  void shouldListAllUsers() throws Exception {

    mockMvc.perform(get(URI))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.*", hasSize(3)))
        .andDo(print());
  }

  @WithUserDetails
  @Test
  void shouldListAllUsersWithFirstNameFilter() throws Exception {

    mockMvc.perform(get(URI)
        .param("firstName", "Wraith"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.*", hasSize(1)));
  }

  @WithUserDetails
  @Test
  void shouldListAllUsersWithCountryFilter() throws Exception {

    mockMvc.perform(get(URI)
        .param("country", "GB"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.*", hasSize(2)));
  }

  @WithUserDetails
  @Test
  void shouldListAllUsersWithNickNameFilter() throws Exception {

    mockMvc.perform(get(URI)
        .param("nickName", "Fu"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.*", hasSize(1)));
  }

  @WithUserDetails
  @Test
  void shouldListAllUsersWithEmailFilter() throws Exception {

    mockMvc.perform(get(URI)
        .param("email", "keeper"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.*", hasSize(1)));
  }

  @WithUserDetails
  @Test
  void shouldUpdateUserThenRetrieve() throws Exception {

    User user = userRepository.save(newUser());
    UserResource resourceToUpdate = UserResource.newUserResource(
        user.getFirstName(),
        user.getLastName(),
        user.getNickName(),
        user.getPassword(),
        user.getEmail(),
        "BR");
    MvcResult result = mockMvc.perform(put(URI + "/{uuid}", user.getUuid().toString())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .content(objectMapper.writeValueAsString(resourceToUpdate)))
        .andExpect(status().isOk())
        .andReturn();
    String location = result.getResponse().getHeader(HttpHeaders.LOCATION);

    assertThat(location, is(notNullValue()));

    mockMvc.perform(get(URI + "/{uuid}", user.getUuid().toString()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.country", equalTo(resourceToUpdate.getCountry())));
  }

  @WithUserDetails
  @Test
  void shouldDeleteUserWhenUuidIsValid() throws Exception {

    List<User> users = userRepository.findAll();

    mockMvc.perform(delete(URI + "/{uuid}", users.get(0).getUuid().toString()))
        .andExpect(status().isNoContent());

    assertThat(userRepository.findAll().size(), is(users.size() - 1));
  }

  @WithUserDetails
  @Test
  void whenGetUserWithUnkownUuidThen404() throws Exception {

    UUID unknownUuid = UUID.randomUUID();
    mockMvc.perform(get(URI + "/{uuid}", unknownUuid.toString()))
        .andExpect(status().isNotFound());

    Optional<User> userOptional = userRepository.findById(unknownUuid);
    assertThat(userOptional.isEmpty(), is(true));
  }

  @WithUserDetails
  @Test
  void whenGetUserWithValidUuidThen200() throws Exception {

    User user = userRepository.findAll().get(0);
    mockMvc.perform(get(URI + "/{uuid}", user.getUuid().toString()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.email", equalTo(user.getEmail())));
  }
}