package com.faceit.user.repository;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;

@DataJpaTest
@Sql({"/schema.sql", "/data.sql"})
class UserRepositoryTest {

  @Autowired
  private UserRepository repository;

  @Test
  public void shouldReturnTrueForExistentEmail() {

    boolean exist = repository.existsUserByEmail("furion322@dota.com");
    assertThat(exist, is(true));
  }

  @Test
  public void shouldReturnFalseForMissingEmail() {

    boolean exist = repository.existsUserByEmail("nature322@dota.com");
    assertThat(exist, is(false));
  }

  @Test
  public void shouldReturnTrueForExistentNickName() {

    boolean exist = repository.existsUserByNickName("Furion");
    assertThat(exist, is(true));
  }

  @Test
  public void shouldReturnFalseForMissingNickName() {

    boolean exist = repository.existsUserByNickName("Lina");
    assertThat(exist, is(false));
  }

}