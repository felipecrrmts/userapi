package com.faceit.user.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import com.faceit.user.exception.TakenException;
import com.faceit.user.exception.UserNotFoundException;
import com.faceit.user.model.User;
import com.faceit.user.repository.UserRepository;
import com.faceit.user.repository.UserSpecification;
import com.faceit.user.resource.SearchCriteria;
import com.faceit.user.resource.UserResource;
import com.faceit.user.service.mapper.UserMapper;
import com.faceit.user.event.UserEvent;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Sort;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

  @Mock
  private UserRepository userRepository;
  @Mock
  private UserMapper userMapper;
  @Mock
  private ApplicationEventPublisher applicationEventPublisher;
  @InjectMocks
  private UserServiceImpl userService;
  @Captor
  protected ArgumentCaptor<Object> publishEventCaptor;

  private UserResource getUserResource() {
    return UserResource.newUserResource(
        "Bone",
        "Fletcher",
        "Clinkz",
        "asdasdasd",
        "little.sk@dota.com",
        "CN");
  }

  private User getUser(UUID uuid) {
    return new User(
        uuid,
        "Bone",
        "Fletcher",
        "Clinkz",
        "asdasdasd",
        "little.sk@dota.com",
        "BR");
  }

  @Test
  public void whenCreateUserReceivesValidUserThenSave() {

    UserResource userResource = getUserResource();

    User newUser = new User(
        null,
        "Bone",
        "Fletcher",
        "Clinkz",
        "asdasdasd",
        "little.sk@dota.com",
        "BR");

    User savedUser = getUser(UUID.randomUUID());

    when(userMapper.mapToNew(userResource)).thenReturn(newUser);
    when(userRepository.existsUserByNickName(userResource.getNickName())).thenReturn(false);
    when(userRepository.existsUserByEmail(userResource.getEmail())).thenReturn(false);
    when(userRepository.save(newUser)).thenReturn(savedUser);

    UUID uuid = userService.create(userResource);

    verify(userRepository).existsUserByNickName(userResource.getNickName());
    verify(userRepository).existsUserByEmail(userResource.getEmail());
    verify(userRepository, times(1)).save(newUser);
    verify(userMapper).mapToNew(userResource);
    verifyNoMoreInteractions(userRepository);
    verifyNoMoreInteractions(userMapper);

    assertThat(uuid, is(savedUser.getUuid()));
  }

  @Test
  public void whenCreateUserWithTakenEmailThenThrowTakenException() {

    UserResource userResource = getUserResource();

    when(userRepository.existsUserByNickName(userResource.getNickName())).thenReturn(false);
    when(userRepository.existsUserByEmail(userResource.getEmail())).thenReturn(true);

    TakenException takenException =
        Assertions.assertThrows(TakenException.class,
            () -> userService.create(userResource));

    assertThat(takenException, notNullValue());
    assertThat(takenException.getErrorsAsMessages().size(), is(1));
    verify(userRepository).existsUserByNickName(userResource.getNickName());
    verify(userRepository).existsUserByEmail(userResource.getEmail());
    verifyNoMoreInteractions(userRepository);
    verifyNoInteractions(userMapper);
  }

  @Test
  public void whenCreateUserWithTakenNickNameThenThrowTakenException() {

    UserResource userResource = getUserResource();

    when(userRepository.existsUserByNickName(userResource.getNickName())).thenReturn(true);
    when(userRepository.existsUserByEmail(userResource.getEmail())).thenReturn(false);

    TakenException takenException =
        Assertions.assertThrows(TakenException.class,
            () -> userService.create(userResource));

    assertThat(takenException, notNullValue());
    assertThat(takenException.getErrorsAsMessages().size(), is(1));
    verify(userRepository).existsUserByNickName(userResource.getNickName());
    verify(userRepository).existsUserByEmail(userResource.getEmail());
    verifyNoMoreInteractions(userRepository);
    verifyNoInteractions(userMapper);
  }

  @Test
  public void whenCreateUserWithTakenNickNameAndEmailThenThrowTakenException() {

    UserResource userResource = getUserResource();

    when(userRepository.existsUserByNickName(userResource.getNickName())).thenReturn(true);
    when(userRepository.existsUserByEmail(userResource.getEmail())).thenReturn(true);

    TakenException takenException =
        Assertions.assertThrows(TakenException.class,
            () -> userService.create(userResource));

    assertThat(takenException, notNullValue());
    assertThat(takenException.getErrorsAsMessages().size(), is(2));
    verify(userRepository).existsUserByNickName(userResource.getNickName());
    verify(userRepository).existsUserByEmail(userResource.getEmail());
    verifyNoMoreInteractions(userRepository);
    verifyNoInteractions(userMapper);
  }

  @Test
  public void whenUpdateChangingNickNameAndEmailUserReceivesValidUserThenUpdate() {

    UserResource userResource = UserResource.newUserResource(
        "Bone",
        "Fletcher",
        "Clinkzzz",
        "asdasdasd",
        "little.sk@dota2.com",
        "CN");
    UUID uuid = UUID.randomUUID();
    User userToUpdate = new User(
        uuid,
        userResource.getFirstName(),
        userResource.getLastName(),
        userResource.getNickName(),
        userResource.getPassword(),
        userResource.getEmail(),
        userResource.getCountry());

    User user = getUser(uuid);

    when(userRepository.existsUserByNickName(userResource.getNickName())).thenReturn(false);
    when(userRepository.existsUserByEmail(userResource.getEmail())).thenReturn(false);
    when(userRepository.findById(uuid)).thenReturn(Optional.of(user));
    when(userMapper.mapToUser(uuid, userResource)).thenReturn(userToUpdate);
    when(userRepository.save(userToUpdate)).thenReturn(userToUpdate);
    doNothing().when(applicationEventPublisher).publishEvent(any(UserEvent.class));

    UUID returningUuid = userService.update(uuid, userResource);

    assertThat(returningUuid, is(uuid));
    verify(userRepository).existsUserByNickName(userResource.getNickName());
    verify(userRepository).existsUserByEmail(userResource.getEmail());
    verify(userRepository, times(1)).save(userToUpdate);
    verify(userMapper).mapToUser(uuid, userResource);
    verify(applicationEventPublisher).publishEvent(any(UserEvent.class));
    verifyPublishedUserEvent();
    verifyNoMoreInteractions(userRepository);
    verifyNoMoreInteractions(userMapper);
    verifyNoMoreInteractions(applicationEventPublisher);
  }

  protected void verifyPublishedUserEvent() {
    verify(applicationEventPublisher, times(1))
        .publishEvent(publishEventCaptor.capture());
    List<Object> capturedEvents = publishEventCaptor.getAllValues();
    for (Object capturedEvent : capturedEvents) {
      assertThat(capturedEvent, instanceOf(UserEvent.class));
    }
  }

  @Test
  public void whenUpdateNotChangingEmailAndNickNameUserReceivesValidUserThenUpdate() {

    UserResource userResource = UserResource.newUserResource(
        "Bone",
        "Fletcher",
        "Clinkz",
        "asdasdasd",
        "little.sk@dota.com",
        "GB");
    UUID uuid = UUID.randomUUID();
    User userToUpdate = new User(
        uuid,
        userResource.getFirstName(),
        userResource.getLastName(),
        userResource.getNickName(),
        userResource.getPassword(),
        userResource.getEmail(),
        userResource.getCountry());
    User user = getUser(uuid);

    when(userRepository.findById(uuid)).thenReturn(Optional.of(user));
    when(userMapper.mapToUser(uuid, userResource)).thenReturn(userToUpdate);
    when(userRepository.save(userToUpdate)).thenReturn(userToUpdate);
    doNothing().when(applicationEventPublisher).publishEvent(any(UserEvent.class));

    UUID returningUuid = userService.update(uuid, userResource);

    assertThat(returningUuid, is(uuid));
    verify(userRepository).findById(uuid);
    verify(userRepository, times(1)).save(userToUpdate);
    verify(userMapper).mapToUser(uuid, userResource);
    verify(applicationEventPublisher).publishEvent(any(UserEvent.class));
    verifyPublishedUserEvent();
    verifyNoMoreInteractions(userRepository);
    verifyNoMoreInteractions(userMapper);
    verifyNoMoreInteractions(applicationEventPublisher);
  }

  @Test
  public void whenUpdateUserWithTakenNickNameAndEmailThenThrowTakenException() {

    UserResource userResource = UserResource.newUserResource(
        "Bone",
        "Fletcher",
        "Clinkzzz",
        "asdasdasd",
        "little.sk@dota2.com",
        "CN");
    UUID uuid = UUID.randomUUID();
    when(userRepository.findById(uuid)).thenReturn(Optional.of(getUser(uuid)));
    when(userRepository.existsUserByNickName(userResource.getNickName())).thenReturn(true);
    when(userRepository.existsUserByEmail(userResource.getEmail())).thenReturn(true);

    TakenException takenException =
        Assertions.assertThrows(TakenException.class,
            () -> userService.update(uuid, userResource));

    assertThat(takenException, notNullValue());
    assertThat(takenException.getErrorsAsMessages().size(), is(2));
    verify(userRepository).findById(uuid);
    verify(userRepository).existsUserByNickName(userResource.getNickName());
    verify(userRepository).existsUserByEmail(userResource.getEmail());
    verifyNoMoreInteractions(userRepository);
    verifyNoInteractions(userMapper);
  }

  @Test
  public void whenUpdateUserWithUnknownUuidThenThrowUserNotFoundException() {

    UserResource userResource = getUserResource();
    UUID uuid = UUID.randomUUID();

    when(userRepository.findById(uuid)).thenReturn(Optional.empty());

    UserNotFoundException userNotFoundException =
        Assertions.assertThrows(UserNotFoundException.class,
            () -> userService.update(uuid, userResource));

    assertThat(userNotFoundException, notNullValue());
    verify(userRepository).findById(uuid);
    verifyNoMoreInteractions(userRepository);
    verifyNoInteractions(userMapper);
  }

  @Test
  public void whenOneUserReceivesValidUuidThenGet() {

    UUID uuid = UUID.randomUUID();
    User user = getUser(uuid);
    when(userRepository.findById(uuid)).thenReturn(Optional.of(user));
    when(userMapper.mapToUserResource(user)).thenReturn(getUserResource());

    UserResource userResource = userService.one(uuid);

    assertThat(userResource, notNullValue());
    verify(userRepository).findById(uuid);
    verify(userMapper).mapToUserResource(user);
    verifyNoMoreInteractions(userRepository);
    verifyNoMoreInteractions(userMapper);
  }

  @Test
  public void whenOneUserReceivesUnknownUuidThenThrowUserNotFoundException() {

    UUID uuid = UUID.randomUUID();
    when(userRepository.findById(uuid)).thenReturn(Optional.empty());

    UserNotFoundException userNotFoundException =
        Assertions.assertThrows(UserNotFoundException.class,
            () -> userService.one(uuid));

    assertThat(userNotFoundException, notNullValue());
    verify(userRepository).findById(uuid);
    verifyNoMoreInteractions(userRepository);
    verifyNoInteractions(userMapper);
  }

  @Test
  public void whenDeleteUserReceivesValidUuidThenDelete() {

    UUID uuid = UUID.randomUUID();
    User user = getUser(uuid);
    when(userRepository.findById(uuid)).thenReturn(Optional.of(user));
    doNothing().when(userRepository).delete(user);

    userService.delete(uuid);

    verify(userRepository).findById(uuid);
    verify(userRepository).delete(user);
    verifyNoMoreInteractions(userRepository);
  }

  @Test
  public void whenDeleteUserReceivesUnknownUuidThenThrowUserNotFoundException() {

    UUID uuid = UUID.randomUUID();
    when(userRepository.findById(uuid)).thenReturn(Optional.empty());

    UserNotFoundException userNotFoundException =
        Assertions.assertThrows(UserNotFoundException.class,
            () -> userService.delete(uuid));

    assertThat(userNotFoundException, notNullValue());
    verify(userRepository).findById(uuid);
    verifyNoMoreInteractions(userRepository);
  }

  @Test
  public void whenAllUserWithNoFiltersThenReturnAll() {

    SearchCriteria searchCriteria =
        new SearchCriteria(null, null, null, null, null);

    List<User> users =
        List.of(getUser(UUID.randomUUID()), getUser(UUID.randomUUID()), getUser(UUID.randomUUID()));

    when(userRepository.findAll(any(UserSpecification.class), any(Sort.class))).thenReturn(users);
    when(userMapper.mapToUserResource(any(User.class))).thenReturn(getUserResource());
    List<UserResource> userResources = userService.all(searchCriteria);

    assertThat(userResources, is(notNullValue()));
    assertThat(userResources.size(), is(3));
    verify(userRepository).findAll(any(UserSpecification.class), any(Sort.class));
    verify(userMapper, times(3)).mapToUserResource(any(User.class));
    verifyNoMoreInteractions(userRepository);
    verifyNoMoreInteractions(userMapper);
  }

  @Test
  public void whenAllUserWithUnkownFilterCombinationThenReturnEmptyUserList() {

    SearchCriteria searchCriteria =
        new SearchCriteria(
            "Bone",
            "Fletcher",
            "Clinkz",
            "little.sk@dota.com",
            "BR");

    when(userRepository.findAll(any(UserSpecification.class), any(Sort.class)))
        .thenReturn(Collections.emptyList());

    List<UserResource> userResources = userService.all(searchCriteria);

    assertThat(userResources, is(empty()));
    assertThat(userResources.size(), is(0));
    verify(userRepository).findAll(any(UserSpecification.class), any(Sort.class));
    verifyNoMoreInteractions(userRepository);
    verifyNoInteractions(userMapper);
  }

}