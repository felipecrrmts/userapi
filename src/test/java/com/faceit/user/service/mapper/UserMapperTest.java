package com.faceit.user.service.mapper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

import com.faceit.user.model.User;
import com.faceit.user.resource.UserResource;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

class UserMapperTest {

  private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
  private UserMapper userMapper = new UserMapper(passwordEncoder);

  private UserResource getUserResource() {
    return UserResource.newUserResource(
        "Bone",
        "Fletcher",
        "Clinkz",
        "asdasdasd",
        "little.sk@dota.com",
        "BR");
  }

  private User getUser() {
    return new User(
        UUID.randomUUID(),
        "Bone",
        "Fletcher",
        "Clinkz",
        "asdasdasd",
        "little.sk@dota.com",
        "BR");
  }

  @Test
  void shouldMapToUserResource() {
    User user = getUser();
    UserResource userResource = userMapper.mapToUserResource(user);

    assertThat(userResource.getFirstName(), is(user.getFirstName()));
    assertThat(userResource.getLastName(), is(user.getLastName()));
    assertThat(userResource.getNickName(), is(user.getNickName()));
    assertThat(userResource.getEmail(), is(user.getEmail()));
    assertThat(userResource.getCountry(), is(user.getCountry()));
    assertThat(userResource.getPassword(), is(user.getPassword()));
  }

  @Test
  void shouldMapToNewUser() {
    UserResource userResource = getUserResource();
    User user = userMapper.mapToNew(userResource);

    assertThat(user.getUuid(), is(nullValue()));
    assertThat(user.getFirstName(), is(userResource.getFirstName()));
    assertThat(user.getLastName(), is(userResource.getLastName()));
    assertThat(user.getNickName(), is(userResource.getNickName()));
    assertThat(user.getEmail(), is(userResource.getEmail()));
    assertThat(user.getCountry(), is(userResource.getCountry()));
    assertThat(
        passwordEncoder.matches(userResource.getPassword(), user.getPassword()),
        is(true));
  }

  @Test
  void shouldMapToUser() {
    UserResource userResource = getUserResource();
    User userToMerge = getUser();
    User user = userMapper.mapToUser(userToMerge.getUuid(), userResource);

    assertThat(user.getUuid(), is(userToMerge.getUuid()));
    assertThat(user.getFirstName(), is(userResource.getFirstName()));
    assertThat(user.getLastName(), is(userResource.getLastName()));
    assertThat(user.getNickName(), is(userResource.getNickName()));
    assertThat(user.getEmail(), is(userResource.getEmail()));
    assertThat(user.getCountry(), is(userResource.getCountry()));
    assertThat(
        passwordEncoder.matches(userResource.getPassword(), user.getPassword()),
        is(true));
  }
}