package com.faceit.user.subscription.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.faceit.user.resource.UserResource;
import com.faceit.user.event.UserEvent;
import com.faceit.user.service.SseServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationEventPublisher;

@SpringBootTest
class SseServiceTest {

  @Autowired
  private ApplicationEventPublisher applicationEventPublisher;
  @MockBean
  private SseServiceImpl sseService;

  @Test
  public void whenUserEventIsPublishedShouldReceiveOnPublish() {
    UserResource userResource = UserResource.newUserResource(
        "Bone",
        "Fletcher",
        "Clinkzzz",
        "asdasdasd",
        "little.sk@dota2.com",
        "CN");
    UserEvent userEvent = new UserEvent(userResource);
    applicationEventPublisher.publishEvent(userEvent);

    verify(sseService, times(1)).onPublish(userEvent);
  }

}