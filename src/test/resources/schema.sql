CREATE TABLE if not exists `user`
(
    `uuid` uuid PRIMARY KEY,
    `first_name` varchar(20)  not null,
    `last_name`  varchar(20)  not null,
    `nick_name`  varchar(20)  not null,
    `password`   varchar(128) null,
    `email`      varchar(128) null,
    `country`    varchar(2)   null
);

